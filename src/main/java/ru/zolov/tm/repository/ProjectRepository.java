package ru.zolov.tm.repository;

import ru.zolov.tm.api.IProjectRepository;
import ru.zolov.tm.entity.Project;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void persist(final Project project) {
        storage.put(project.getId(), project);
    }

    @Override
    public Project findOne(final String userId, final String id) throws Exception {
        Project project = storage.get(id);
        if (!project.getUserId().equals(userId)) throw new Exception("Wrong user! Permission denied!");
        return project;
    }

    @Override
    public List<Project> findAll(final String userId) {
        List<Project> projectList = new ArrayList<>();
        for (Project project : storage.values()) {
            if (project.getUserId().equals(userId)) {
                projectList.add(project);
            }
        }
        return projectList;
    }

    @Override
    public List<Project> findAllByUserId(final String userId) {
        List<Project> projectList = new ArrayList<>();
        for (Project project : storage.values()) {
            if (project.getUserId().equals(userId)) projectList.add(project);
        }
        return projectList;
    }

    @Override
    public void update(final String userId, final String id, final String name, final String description, Date start, Date finish) {
        final Project project = storage.get(id);
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateOfStart(start);
        project.setDateOfFinish(finish);
    }

    @Override
    public boolean remove(final String userId, final String id) {
        if (storage.get(id).getUserId().equals(userId)) {
            storage.remove(id);
            return true;
        }
        return false;
    }

    @Override
    public void removeAll() {
        storage.clear();
    }

    @Override
    public void merge(final Project project) {
        if (project == null) return;
        if (storage.containsKey(project.getId())) {
            update(project.getUserId(), project.getId(), project.getName(), project.getDescription(), project.getDateOfStart(), project.getDateOfFinish());
        } else {
            persist(project);
        }
    }
}
