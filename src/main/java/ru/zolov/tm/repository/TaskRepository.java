package ru.zolov.tm.repository;

import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void persist(final String userId, final String id, final String name) {
        final Task task = new Task(userId, id);
        task.setName(name);
        storage.put(task.getId(), task);
    }

    @Override
    public void persist(final Task task) {
        storage.put(task.getId(), task);
    }

    @Override
    public Task findOne(final String id) {
        return storage.get(id);
    }
    public Task findOne(final String userId, final String id) throws Exception {
        Task task = storage.get(id);
        if (!task.getUserId().equals(userId)) throw new Exception("Wrong user! Permission denied!");
        return task;
    }

    @Override
    public List<Task> findAll() {
        return new ArrayList<>(storage.values());

    }

    @Override
    public List<Task> findTaskByProjId(final String id) throws Exception {
        List<Task> result = null;

        for (Task task : storage.values()) {
            if (task == null) throw new Exception("FindTaskByProjId produce exception");
            if (task.getProjectId().equals(id)) {
                result.add(task);
            }
        }
        return result = new ArrayList<>();
    }

    @Override
    public List<Task> findAllByUserId(final String userId) {
        List<Task> taskList = new ArrayList<>();
        for (Task task : storage.values()) {
            if (task.getUserId().equals(userId)) taskList.add(task);
        }
        return taskList;
    }

    @Override
    public void update(final String id, final String description) {
        final Task task = storage.get(id);
        task.setName(description);
    }

    @Override
    public boolean remove(final String id) {
        return storage.remove(id) != null;
    }

    @Override
    public void removeAll() {
        storage.clear();
    }

    @Override
    public boolean removeAllByProjectID(final String userId, final String id) {
        for (Task task : storage.values()) {
            if (task.getUserId().equals(userId) && task.getProjectId().equals(id)) {
                storage.remove(task.getId());
                return true;
            }
        }
        return false;
    }

    @Override
    public void merge(final Task task) {
        if (task == null) return;
        if (storage.containsKey(task.getId())) {
            update(task.getId(), task.getDescription());
        } else {
            persist(task);
        }
    }
}


