package ru.zolov.tm.repository;

import java.util.LinkedHashMap;
import java.util.Map;

abstract class AbstractRepository<E> {
    Map<String, E> storage = new LinkedHashMap<>();

    abstract void persist(final E entity);

    abstract void merge(final E entity);

}
