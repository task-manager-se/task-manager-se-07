package ru.zolov.tm.command.task;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.security.RoleType;

public final class TaskRemoveCommand extends AbstractCommand {
    private final String name = "task-remove";
    private final String description = "Remove task";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        for (Task task : serviceLocator.getTaskService().readAll()) {
            System.out.println(task);
        }

        System.out.print("Enter task id: ");
        final String id = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getTaskService().remove(id);
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }
}
