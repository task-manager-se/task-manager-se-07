package ru.zolov.tm.command.task;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.security.RoleType;

public final class TaskCreateCommand extends AbstractCommand {
    private final String name = "task-create";
    private final String description = "Create new task";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        final String user = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getTaskService().readAll();
        System.out.print("Enter project id: ");
        final String projectId = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getTaskService().create(user, projectId);
        serviceLocator.getTaskService().readTaskByProjectId(projectId);
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }
}
