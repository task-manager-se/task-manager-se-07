package ru.zolov.tm.command.project;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.security.RoleType;

import java.util.Date;

public final class ProjectEditCommand extends AbstractCommand {
    private final String name = "project-edit";
    private final String description = "Edit project";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getProjectService().readAll(userId);
        System.out.print("Enter project id: ");
        final String id = serviceLocator.getTerminalService().nextLine();

        System.out.print("Enter new description: ");
        final String description = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getProjectService().update(userId, id, name, description, new Date(), new Date());
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }
}
