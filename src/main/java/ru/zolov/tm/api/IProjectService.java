package ru.zolov.tm.api;

import ru.zolov.tm.entity.Project;

import java.util.Date;
import java.util.List;

public interface IProjectService {
    IProjectRepository getProjectRepository();

    ITaskRepository getTaskRepository();

    void create(final String userId, final String name, final String description, Date start, Date finish) throws Exception;

    Project read(final String userId, final String id) throws Exception;

    List<Project> readAll(final String userId);

    void update(final String userId, final String id, final String name, final String description, Date start, Date finish);

    boolean remove(final String userId, final String id) throws Exception;
}
