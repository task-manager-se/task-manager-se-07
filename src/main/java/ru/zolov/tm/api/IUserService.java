package ru.zolov.tm.api;

import ru.zolov.tm.entity.User;
import ru.zolov.tm.security.RoleType;

public interface IUserService {
    User getCurrentUser();

    void setCurrentUser(final User currentUser);

    boolean isAuth();

    User login(final String login, final String password) throws Exception;

    void userRegistration(final String login, final String password) throws Exception;

    void adminRegistration(final String login, final String password) throws Exception;

    void logOut();

    boolean isRolesAllowed(final RoleType... roleTypes);

    User findByLogin(final String login) throws Exception;

    boolean remove(final String id) throws Exception;

    void updateUserPassword(final String id, final String password) throws Exception;
}
