package ru.zolov.tm.api;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.service.TerminalService;

import java.util.List;

public interface ServiceLocator {
    IProjectService getProjectService();

    ITaskService getTaskService();

    TerminalService getTerminalService();

    List<AbstractCommand> getCommandList();

    IUserService getUserService();
}
