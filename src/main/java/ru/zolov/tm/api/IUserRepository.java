package ru.zolov.tm.api;

import ru.zolov.tm.entity.User;

import java.util.List;

public interface IUserRepository {
    void persist(final User user);

    List<User> findAll();

    User findOne(final String id);

    User findByLogin(final String login);

    void merge(final User user);

    boolean remove(final String id);

    void removeAll();

    void update(final String id, final String login);
}
