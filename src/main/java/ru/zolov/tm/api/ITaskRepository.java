package ru.zolov.tm.api;

import ru.zolov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {
    void persist(final String userId, final String id, final String name);

    void persist(final Task task);

    Task findOne(final String id);

    List<Task> findAll();

    List<Task> findTaskByProjId(final String id) throws Exception;

    List<Task> findAllByUserId(final String userId);

    void update(final String id, final String description);

    boolean remove(final String id);

    void removeAll();

    boolean removeAllByProjectID(final String userId, final String id);

    void merge(final Task task);
}
