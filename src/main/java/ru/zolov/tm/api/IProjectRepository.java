package ru.zolov.tm.api;

import ru.zolov.tm.entity.Project;

import java.util.Date;
import java.util.List;

public interface IProjectRepository {
    void persist(final Project project);

    Project findOne(final String userId, final String id) throws Exception;

    List<Project> findAll(final String userId);

    List<Project> findAllByUserId(final String userId);

    void update(final String userId, final String id, final String name, final String description, Date start, Date finish);

    boolean remove(final String userId, final String id);

    void removeAll();

    void merge(final Project project);
}
