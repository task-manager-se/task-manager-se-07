package ru.zolov.tm.api;

import ru.zolov.tm.entity.Task;

import java.util.List;

public interface ITaskService {
    ITaskRepository getTaskRepository();

    void create(final String userId, final String name) throws Exception;

    List<Task> readAll() throws Exception;

    List<Task> readTaskByProjectId(final String id) throws Exception;

    Task readTaskById(final String id);

    boolean remove(final String id) throws Exception;

    void update(final String id, final String description) throws Exception;
}
