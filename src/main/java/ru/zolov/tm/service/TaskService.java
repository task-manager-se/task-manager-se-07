package ru.zolov.tm.service;

import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.api.ITaskService;
import ru.zolov.tm.entity.Task;

import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {
    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public ITaskRepository getTaskRepository() {
        return taskRepository;
    }

    @Override
    public void create(final String userId, final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception("Name input empty");
        taskRepository.persist(new Task(userId, name));
    }

    @Override
    public List<Task> readAll() throws Exception {
        if (taskRepository.findAll() == null || taskRepository.findAll().isEmpty())
            throw new Exception("Empty storage");
        return taskRepository.findAll();
    }

    @Override
    public List<Task> readTaskByProjectId(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception("Id input empty");
        return taskRepository.findTaskByProjId(id);
    }

    @Override
    public Task readTaskById(final String id) {
        return taskRepository.findOne(id);
    }

    @Override
    public boolean remove(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception("Id input empty");
        return taskRepository.remove(id);
    }

    @Override
    public void update(final String id, final String description) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception("Id input empty");
        taskRepository.update(id, description);
    }

}
