package ru.zolov.tm.service;

import ru.zolov.tm.api.IUserRepository;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.security.RoleType;
import ru.zolov.tm.util.MD5Hash;

import java.util.Arrays;
import java.util.List;

public class UserService extends AbstractService<User> implements IUserService {
    private final IUserRepository userRepository;
    private User currentUser = null;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public boolean isAuth() {
        return currentUser != null;
    }

    @Override
    public User login(String login, String password) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception("Empty login");
        if (password == null || password.isEmpty()) throw new Exception("Empty password");
        User user = userRepository.findByLogin(login);
        if (user == null) throw new Exception("UserNotFound");
        String passwordHash = MD5Hash.getHash(password);
        if (passwordHash.equals(user.getPasswordHash())) return user;
        else return null;
    }

    @Override
    public void userRegistration(final String login, final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception("Empty login");
        if (password == null || password.isEmpty()) throw new Exception("Empty password");
        if (userRepository.findByLogin(login) != null) throw new Exception("User not found");
        User newUser = new User(login);
        newUser.setRole(RoleType.USER);
        newUser.setPasswordHash(MD5Hash.getHash(password));
        userRepository.persist(newUser);
    }

    @Override
    public void adminRegistration(final String login, final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception("Empty login");
        if (password == null || password.isEmpty()) throw new Exception("Empty password");
        if (userRepository.findByLogin(login) != null) throw new Exception("User already exist");
        User admin = new User(login);
        admin.setRole(RoleType.ADMIN);
        admin.setPasswordHash(MD5Hash.getHash(password));
        userRepository.persist(admin);
    }

    @Override
    public void logOut() {
        currentUser = null;
    }

    @Override
    public boolean isRolesAllowed(final RoleType... roleTypes) {
        if (roleTypes == null) return false;
        if (currentUser == null || currentUser.getRole() == null) return false;
        final List<RoleType> types = Arrays.asList(roleTypes);
        return types.contains(currentUser.getRole());
    }

    @Override
    public User findByLogin(final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception("Incorrect login");
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean remove(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception("Id input empty");
        userRepository.remove(id);
        return userRepository.remove(id);
    }

    @Override
    public void updateUserPassword(final String id, final String password) throws Exception {
        User user = userRepository.findOne(id);
        if (id == null || id.isEmpty()) throw new Exception("Id input empty");
        if (password == null || password.isEmpty()) throw new Exception("Password input empty");
        user.setPasswordHash(MD5Hash.getHash(password));
        userRepository.merge(user);
    }

}
