package ru.zolov.tm.service;

import ru.zolov.tm.api.IProjectRepository;
import ru.zolov.tm.api.IProjectService;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.entity.Project;

import java.util.Date;
import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;
    private final ITaskRepository taskRepository;

    public ProjectService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public IProjectRepository getProjectRepository() {
        return projectRepository;
    }

    @Override
    public ITaskRepository getTaskRepository() {
        return taskRepository;
    }

    @Override
    public void create(final String userId, final String name, final String description, Date start, Date finish) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception("UserID input empty");
        if (name == null || name.isEmpty()) throw new Exception("Name input empty");
        final Project project = new Project(name);
        project.setUserId(userId);
        project.setDescription(description);
        project.setDateOfStart(start);
        project.setDateOfFinish(finish);
        if (description == null || description.isEmpty()) project.setDescription("empty");
        projectRepository.persist(project);
    }

    @Override
    public Project read(final String userId, final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception("Empty input");
        if (id == null || id.isEmpty()) throw new Exception("Empty input");
        return projectRepository.findOne(userId, id);
    }

    @Override
    public List<Project> readAll(final String userId) {
        return projectRepository.findAll(userId);
    }

    @Override
    public void update(final String userId, final String id, final String name, final String description, Date start, Date finish) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        if (name == null || name.isEmpty()) return;

        projectRepository.update(userId, id, name, description, start, finish);
    }

    @Override
    public boolean remove(final String userId, final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception("Empty input");
        if (id == null || id.isEmpty()) throw new Exception("Id input empty");
        taskRepository.removeAllByProjectID(userId, id);
        return projectRepository.remove(userId, id);
    }
}
