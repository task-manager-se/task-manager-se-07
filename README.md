# Project description

Welcome to the home page of the Task Manager project.

## Software requirements

Java 1.8

## Technology stack
- Java 1.8
- Maven 4
- JUnit 4
- SLF4J 1.7.5
- JCabi 1.1

## Help

  Check "*help*" for display task manager commands

## Build

  ```bash
mvn clean install
  ```

## Run


  ```bash
  java -jar C:\path\to\folder\task-manager-se-02-1.0-SNAPSHOT.jar
  ```

## CI config

```yaml
services:
    - docker:dind
    
variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    
build:
    image: maven:3-jdk-8
    stage: build
    script:
        - mvn clean install
    artifacts:
        paths:
            - target/*.jar

```



## Contacts 
**Developers:**

**Igor Zolov** 

- **email**: i.zolov@yandex.ru

  

